#include<iostream>
#include <stdlib.h>     
#include <time.h> 
#include<vector>
#include "Class.h"
using namespace std;

//Function for random the number of customer.
int random_Customer(int max, int min)
{
	int customer = rand() % (max - min + 1) + min; //random the number.

	return customer;
}

//Function for sort the time of customer.
void Sorting_Customer(vector<Customer>& customer, int size)
{
	Customer hold;
	int swap;

	do 
	{
		swap = 0;
		for (int i = 0; i < size - 1; i++)
		{
			if (customer[i].Arrived_Time > customer[i + 1].Arrived_Time)
			{
				hold = customer[i];
				customer[i] = customer[i + 1];
				customer[i + 1] = hold;
				swap = 1;
			}
		}
	} while (swap == 1); //bubble sort
}

int main() {
	
	////////////Variable////////////
	srand(time(0));
	vector<Customer> bankCustomers;
	vector<Cashier> bankCashiers;
	int Max_Customer, Min_Customer, Max_Servicetime,
		Min_Servicetime, Num_of_Cashiers, Num_of_Customer;
	float Average = 0;
	////////////////////////////////

	cout << "----------------------------------------\n" 
		 << "\t\tBankSim" 
		 << "\n----------------------------------------\n";

	//////////Users prompt//////////
	cout << "Max of Customer : ";
	cin >> Max_Customer;
	cout << "Min of Customer : ";
	cin >> Min_Customer;
	cout << "Max of ServiceTime : ";
	cin >> Max_Servicetime;
	cout << "Min of ServiceTime : ";
	cin >> Min_Servicetime;
	cout << "Number of Cashiers : ";
	cin >> Num_of_Cashiers;
	////////////////////////////////

	Num_of_Customer = random_Customer(Max_Customer, Min_Customer); //Random number of customer from Max and Min customer that user prompt and return it.
	cout << "Number of Customer : " << Num_of_Customer; //Print the number of customer after random some number.

	//Set the data of customers.
	for (int i = 0; i < Num_of_Customer; i++) 
	{
		Customer customer;
		customer.Arrived_Time = rand() % 241; //random the number of time that customer arrive from 0 until 240 minutes or 4hours.
		customer.Left_Time = 0;
		customer.Service_Time = random_Customer(Max_Servicetime, Min_Servicetime); //random number of service time from Max and Min service time that user prompt and return it.
		customer.Waiting_Time = 0;
		bankCustomers.push_back(customer); //add data into the vector.
	}

	Sorting_Customer(bankCustomers, Num_of_Customer); //Sorting the number of time that customer arrive.

	//Set the cashier time left.
	for (int i = 0; i < Num_of_Cashiers; i++) 
	{
		Cashier cashier;
		cashier.Left_Time = 0;
		bankCashiers.push_back(cashier); //add data into the vector.
	}
	
	//Queue in the bank.
	for (int i = 0; i < Num_of_Customer; i++) 
	{
		//If number of customer less than number of cashier
		if (i < Num_of_Cashiers) 
		{ 
			bankCustomers[i].Waiting_Time = 0; //set waiting time equal to 0.
			bankCustomers[i].Left_Time = bankCustomers[i].Arrived_Time + bankCustomers[i].Service_Time + bankCustomers[i].Waiting_Time; //calculate the left time.
			bankCashiers[i].Left_Time = bankCustomers[i].Left_Time; //set the cashier left time equal to the customer left time.
		}
		else 
		{ 
			int WaitingTime;
			int Number_Cashier;

			//Check the cashier No.0 if it have waiting time.
			if (bankCashiers[0].Left_Time > bankCustomers[i].Arrived_Time) 
			{ 
				WaitingTime = bankCashiers[0].Left_Time - bankCustomers[i].Arrived_Time; ///calculate the waiting time
				Number_Cashier = 0;
			}
			//If it don't have waiting time.
			else 
			{ 
				WaitingTime = 0;
				Number_Cashier = 0;
			}

			//In the other No. of cashier.
			for (int j = 1; j < Num_of_Cashiers; j++) 
			{
				//Check if it have the waiting time.
				if (bankCashiers[j].Left_Time > bankCustomers[j].Arrived_Time) 
				{ 
					//Check the waiting time of cashier No.j less than holded waiting time
					if (bankCashiers[j].Left_Time - bankCustomers[i].Arrived_Time < WaitingTime) 
					{ 
						WaitingTime = bankCashiers[j].Left_Time - bankCustomers[i].Arrived_Time;
						//If the waiting time less than 0  
						if (WaitingTime < 0)
						{
							WaitingTime = 0;//set the waiting time equal to 0;
						}
						Number_Cashier = j;
					}
				}
				//Check if it don't have the waiting time.
				else 
				{ 
					WaitingTime = 0;
					Number_Cashier = j;
				}
			}
			bankCustomers[i].Waiting_Time = WaitingTime;//Set the waiting time.
			bankCustomers[i].Left_Time = bankCustomers[i].Arrived_Time + bankCustomers[i].Service_Time + bankCustomers[i].Waiting_Time; //Calculate the left time.
			bankCashiers[Number_Cashier].Left_Time = bankCustomers[i].Left_Time; //Set the cashier left time.
		}
	}
	
	/////////Display output/////////
	for (int i = 0; i < Num_of_Customer; i++) 
	{
		cout << "\n----------------------------------------\n";
		cout << "\t   Customer No." << i + 1;
		cout << "\n----------------------------------------\n";
		cout << "\nArrived time : " << bankCustomers[i].Arrived_Time;
		cout << "\nWaiting time : " << bankCustomers[i].Waiting_Time;
		cout << "\nLeft time : " << bankCustomers[i].Left_Time; 
		cout << endl;
	}
	///////////////////////////////

	//Calculate the average waiting time in total.
	for (int i = 0; i < Num_of_Customer; i++) {
		Average = Average + bankCustomers[i].Waiting_Time; //calculate the average waiting time. 
	}
	Average = Average / Num_of_Customer; //get the average value after divided by number of customer.
	cout << "\nAverage of Waiting time : " << Average << " mintues";

	return 0;
}





